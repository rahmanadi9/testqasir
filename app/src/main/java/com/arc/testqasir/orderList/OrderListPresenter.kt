package com.arc.testqasir.orderList

import com.arc.qasir_api.BaseResponse
import com.arc.qasir_api.BinRepo
import com.arc.qasir_api.BinResponse
import com.arc.qasir_api.ResponseCourier

class OrderListPresenter(var view : OrderListContract.View): OrderListContract.Presenter, ResponseCourier  {

    var repo: BinRepo = BinRepo(this)

    override fun getOrderList() {
        repo.getBinData()
    }

    override fun getDataResponse(response: BaseResponse<*>?, message: String?) {
        if(response != null && response is BinResponse){
            var data = response as BinResponse
            view.showOrderList(data.data)
        }
    }
}