package com.arc.testqasir.orderList

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arc.qasir_api.ProductModel
import com.arc.testqasir.R
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_product.view.*

class OrderListAdapter(private val items:MutableList<ProductModel>,
                       private val listener: (ProductModel) -> Unit):
                    RecyclerView.Adapter<OrderListAdapter.ViewHolder>(){
    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        context = p0.context
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_product,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(position,items.get(position),listener,context)
    }



    class ViewHolder(override val containerView: View): RecyclerView.ViewHolder(containerView),LayoutContainer{
        fun bindItem(position:Int, data:ProductModel, listener:(ProductModel)->Unit,context: Context){

            Glide.with(context).load(data.images.thumbnail).placeholder(R.drawable.ic_banner_background).into(containerView.findViewById(
                R.id.ivProduct
            ))

            containerView.tvProductName.text = data.product_name
            containerView.tvPrice.text = "Rp. "+data.price
            containerView.tvStock.text = data.stock.toString()

            containerView.cvProduct.setOnClickListener{ listener(data)}
        }
    }
}