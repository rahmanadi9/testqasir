package com.arc.testqasir.orderList

import com.arc.qasir_api.BinModel

public interface OrderListContract {
    interface View{
        fun showOrderList(binData: BinModel)
    }

    interface Presenter{
        fun getOrderList();
    }
}