package com.arc.testqasir.orderList


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arc.qasir_api.BinModel
import com.arc.qasir_api.ProductModel
import com.arc.testqasir.MainActivity
import com.arc.testqasir.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_order_list.*
import kotlinx.android.synthetic.main.fragment_order_list.view.*


class OrderListFragment : Fragment(), OrderListContract.View {


    lateinit var presenter: OrderListPresenter

    var listData: MutableList<ProductModel> = mutableListOf()
    lateinit var listProductAdapter: OrderListAdapter

    companion object{
        fun newInstance(): OrderListFragment {
            val args = Bundle()
            val fragment = OrderListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_order_list, container, false)
        presenter = OrderListPresenter(this)
        presenter.getOrderList()

        listProductAdapter = OrderListAdapter(listData) {
            ((activity) as MainActivity).orderDetailSelected(it)
        }
        v.rvProduct.adapter = listProductAdapter

        return v;
    }

    override fun showOrderList(binData: BinModel) {
        //Log.d("Order",binData.toString())
        Glide.with(context).load(binData.banner.image).placeholder(R.drawable.ic_launcher_background).into(ivBanner)
        listData.clear()
        listData.addAll(binData.products)
        listProductAdapter.notifyDataSetChanged()
    }
}
