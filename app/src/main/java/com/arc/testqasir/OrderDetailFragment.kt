package com.arc.testqasir


import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arc.qasir_api.ProductModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_order_detail.view.*


class OrderDetailFragment : Fragment() {

    lateinit var productData:ProductModel

    companion object{
        var PRODUCT_DATA = "product_data"

        fun newInstance(productData:ProductModel): OrderDetailFragment{
            val args = Bundle()
            val fragment = OrderDetailFragment()
            args.putParcelable(PRODUCT_DATA,productData)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var v = inflater.inflate(R.layout.fragment_order_detail, container, false)

        //get data
        arguments?.let {
            productData = arguments!!.getParcelable(PRODUCT_DATA) as ProductModel
        }?:run{
            Toast.makeText(context,"No data found",Toast.LENGTH_SHORT).show()
        }

        if(::productData.isInitialized){
            Glide.with(context).load(productData.images.large).placeholder(R.drawable.ic_launcher_background).into(v.ivProduct)
            v.tvProductName.text = productData.product_name
            v.tvProductPrice.text = "Rp. "+ productData.price
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                v.tvDesc.text = Html.fromHtml(productData.description,Html.FROM_HTML_MODE_COMPACT)
            }else{
                v.tvDesc.text = Html.fromHtml(productData.description)
            }
        }else{
            Toast.makeText(context,"No Data Found",Toast.LENGTH_SHORT).show()
        }
        /*productData?.let{


        }*/

        return v
    }


}
