package com.arc.testqasir

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.arc.qasir_api.ProductModel
import com.arc.testqasir.orderList.OrderListFragment

class MainActivity : AppCompatActivity() {

    var splashFragment: Fragment? = null
    var orderListFragment: Fragment? = null
    var orderDetailFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //splashSelected()
        orderListSelected()
    }

    fun splashSelected(){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        splashFragment = SplashFragment.newInstance()
        fragmentTransaction.add(R.id.fr,splashFragment!!)
        fragmentTransaction.addToBackStack("Splash")
        fragmentTransaction.commit()
    }

    fun orderListSelected(){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        orderListFragment = OrderListFragment.newInstance()
        fragmentTransaction.add(R.id.fr,orderListFragment!!)
        fragmentTransaction.addToBackStack("OrderList")
        fragmentTransaction.commit()
    }

    fun orderDetailSelected(productData: ProductModel){
        if(productData == null){
            Toast.makeText(this@MainActivity,"Data null",Toast.LENGTH_SHORT).show()
        }else{
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            orderDetailFragment = OrderDetailFragment.newInstance(productData)
            fragmentTransaction.add(R.id.fr,orderDetailFragment!!)
            fragmentTransaction.addToBackStack("OrderDetail")
            fragmentTransaction.commit()
        }

    }

}
