package com.arc.qasir_api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class QasirClient {

    public static final String BASE_URL = "https://api.myjson.com/bins/";

    private static QasirClient instance;
    private static Retrofit retrofit;

    private static BinService binService;

    private QasirClient(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

    }

    public static QasirClient getInstance(){
        if(instance == null){
            instance = new QasirClient();
        }
        return instance;
    }

    public static BinService getBinService(){
        binService = retrofit.create(BinService.class);
        return binService;
    }
}
