package com.arc.qasir_api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BinRepo extends BaseRepo {
    public BinRepo(ResponseCourier courier) {
        super(courier);
    }

    public void getBinData(){
        try{
            QasirClient.getInstance().getBinService().getProductsOnBin(headerRequest)
                    .enqueue(new Callback<BinResponse>() {
                        @Override
                        public void onResponse(Call<BinResponse> call, Response<BinResponse> response) {
                            if(response.body() != null){
                                courier.getDataResponse(response.body(),"sukses");
                            }else{
                                courier.getDataResponse(null,"failed get data");
                            }
                        }

                        @Override
                        public void onFailure(Call<BinResponse> call, Throwable t) {
                            courier.getDataResponse(null, t.getMessage());
                        }
                    });
        }catch (Exception e){
            courier.getDataResponse(null,e.getMessage());
        }
    }
}
