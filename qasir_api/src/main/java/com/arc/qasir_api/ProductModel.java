package com.arc.qasir_api;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductModel implements Parcelable {
    public int product_id;
    public String product_name;
    public int price;
    public int stock;
    public String description;
    public Image images;

    public class Image{
        public String thumbnail;
        public String large;
    }

    public ProductModel(){}

    protected ProductModel(Parcel in) {
        product_id = in.readInt();
        product_name = in.readString();
        price = in.readInt();
        stock = in.readInt();
        description = in.readString();
        images = (Image) in.readValue(Image.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(product_id);
        dest.writeString(product_name);
        dest.writeInt(price);
        dest.writeInt(stock);
        dest.writeString(description);
        dest.writeValue(images);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ProductModel> CREATOR = new Parcelable.Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };
}