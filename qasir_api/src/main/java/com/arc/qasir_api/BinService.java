package com.arc.qasir_api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;

import java.util.Map;

public interface BinService {

    @GET("gsq5w")
    Call<BinResponse> getProductsOnBin(@HeaderMap Map<String,String> header);
}
