package com.arc.qasir_api;

public class BaseResponse<T> {
    public T data;
    public String message;
    public String status;
}
